This application allows you to detect objects on the frames of a video file. 
It classifies objects from an input video received by HTTP request.


## To start app locally you should follow these steps:

### 1. Setup Docker and Docker Compose
```
https://docs.docker.com/engine/install/
https://docs.docker.com/compose/install/
```


### 2. Get backend repository
##### 2.1 Install Git.
##### 2.2 Create directory and change your current directory to the newly created.
##### 2.3 Clone repository. Perform this command in console:
`git clone https://gitlab.com/test6071434/video-analyzer.git`


### 3. Setup Docker env
Perform next commands in console:
```

# build Docker images
`docker-compose build`

# start Docker containers
`docker-compose up'
```

### 4. Available endpoints

After launching the application, the following endpoints will be available:

- POST http://localhost:5000 - to send video file for processing
- GET http://localhost:5000  - to get list of processed(processing) objects
- GET http://localhost:5000/<file_id>  - to get certain object


For POST request you can use CURL:
```
curl -F "file=@/<PATH_TO_VIDEO_FILE>/<FILE_NAME>.mp4" http://localhost:5000
``` 
