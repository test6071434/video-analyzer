import os
import json
import base64
import uuid

import pika
from flask import Flask, request, jsonify
from flask.views import View
from flask_pymongo import PyMongo

app = Flask(__name__)

app.config["MONGO_URI"] = os.environ['MONGODB_URL']
mongo = PyMongo(app)


class ProcessVideoView(View):

    methods = ["POST"]

    def dispatch_request(self):
        # TODO: validation
        if 'file' in request.files:
            file = request.files['file']

            _id = uuid.uuid4().hex
            mongo.db.videos.insert_one(
                {
                    'file_id': _id,
                    'file_name': file.filename,
                    'status': 'in_progress'
                }
            )

            msg_data = {
                'file_id': _id,
                'file_name': file.filename,
                'content': base64.b64encode(file.stream.read()).decode('utf-8')
            }

            connection = pika.BlockingConnection(pika.ConnectionParameters(host="rabbitmq"))
            channel = connection.channel()
            channel.queue_declare(queue='task_queue', durable=True)
            channel.basic_publish(
                exchange='',
                routing_key='task_queue',
                body=json.dumps(msg_data).encode(),
                properties=pika.BasicProperties(delivery_mode=pika.DeliveryMode.Persistent)
            )
            connection.close()

        return jsonified_response(201)


class GetProcessedVideoListView(View):

    methods = ["GET"]

    def dispatch_request(self):
        # TODO: pagination
        objs = [
            {
                'id': obj.get('file_id'),
                'file_name': obj.get('file_name'),
                'status': obj.get('status'),
                'frames': obj.get('frames')
            } for obj in mongo.db.videos.find()
        ]

        return jsonified_response(200, items=objs)


class GetProcessedVideoRetrieveView(View):

    methods = ["GET"]

    def dispatch_request(self, file_id: str):
        obj = mongo.db.videos.find_one({"file_id": file_id})

        return jsonified_response(
            200,
            **{
                'id': obj.get('file_id'),
                'file_name': obj.get('file_name'),
                'status': obj.get('status'),
                'frames': obj.get('frames')
            }
        )


def jsonified_response(status_code, **kwargs):
    resp = jsonify(**kwargs)
    resp.status_code = status_code
    resp.content_type = 'application/json'

    return resp


app.add_url_rule('/', view_func=ProcessVideoView.as_view("process-video"))
app.add_url_rule('/', view_func=GetProcessedVideoListView.as_view("processed-video-list"))
app.add_url_rule('/<string:file_id>', view_func=GetProcessedVideoRetrieveView.as_view("processed-video-retrieve"))


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
