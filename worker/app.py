import os
import json
import base64


import cv2
import pika
import pymongo
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.data import MetadataCatalog


TEMPORARY_FILE_NAME = 'tmp.mp4'
CONFIG_FILE_NAME = 'COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml'


cfg = get_cfg()

cfg.MODEL.DEVICE = 'cpu'
cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.5
cfg.merge_from_file(model_zoo.get_config_file(CONFIG_FILE_NAME))
cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url(CONFIG_FILE_NAME)

predictor = DefaultPredictor(cfg)

mongo_client = pymongo.MongoClient(os.environ['MONGODB_URL'])
test_db = mongo_client['testdb']


class VideoFileProcessor:

    @classmethod
    def callback(cls, ch, method, properties, msg):
        msg_data = json.loads(msg)
        content = base64.b64decode(msg_data['content'].encode())
        frames_stat = cls.process_file(content)
        test_db.videos.find_one_and_update(
            {'file_id': msg_data['file_id']},
            {'$set': {'status': 'completed', 'frames': frames_stat}}
        )
        ch.basic_ack(delivery_tag=method.delivery_tag)
        print(f"{msg_data['file_name']} processed")

    @classmethod
    def process_file(cls, file: bytes):
        # opencv can't work with file objects
        with open(f'./{TEMPORARY_FILE_NAME}', 'wb') as fp:
            fp.write(file)

        video = cv2.VideoCapture(TEMPORARY_FILE_NAME)

        frame_number = 0
        processed_frames = []
        while True:
            success, im = video.read()
            if not success:
                break

            frame_objects = cls.process_frame(im)
            processed_frames.append({str(frame_number): frame_objects})
            frame_number += 1

        os.remove(f'./{TEMPORARY_FILE_NAME}')
        return processed_frames

    @classmethod
    def process_frame(cls, frame):
        outputs = predictor(frame)
        instances = outputs["instances"].to("cpu")

        predicted_classes = instances.pred_classes.tolist() if instances.has("pred_classes") else []
        scores = [round(float(item), 3) for item in instances.scores] if instances.has("scores") else []
        available_classes = MetadataCatalog.get(cfg.DATASETS.TRAIN[0]).get("thing_classes")

        detected_labels_names = [available_classes[item] for item in predicted_classes]
        return [{cls: score} for cls, score in zip(detected_labels_names, scores)]


def main():
    connection = pika.BlockingConnection(pika.ConnectionParameters(host="rabbitmq"))
    channel = connection.channel()

    channel.queue_declare(queue='task_queue', durable=True)
    channel.basic_consume(queue='task_queue', on_message_callback=VideoFileProcessor.callback)
    channel.start_consuming()


if __name__ == '__main__':
    main()
